# -*- coding: utf-8 -*-
"""
Installation and testing for the ownhero.cloudhelpers library.
"""
from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

IAM_REQUIREMENTS = [
    "google-api-python-client",
    "google-auth",
    "google-auth-httplib2",
    "google-cloud-iam",
    "pyyaml",
]

REDIS_REQUIREMENTS = ["redis"]

POSTGRES_REQUIREMENTS = ["psycopg2-binary"]

GOOGLE_FILESTORAGE_REQUIREMENTS = ["google-cloud-storage"]

RUNTIME_REQUIREMENTS = [
    "google-cloud-pubsub",
    "google-cloud-storage",
] + IAM_REQUIREMENTS

TEST_REQUIREMENTS = ["nose", "coverage"]

DEV_REQUIREMENTS = [
    "autopep8",
    "bandit",
    "black",
    "flake8",
    "notebook",
    "pylint",
    "rope",
]

DOC_REQUIREMENTS = [
    "sphinx",
    "recommonmark",
    "sphinx_rtd_theme",
    "doc8",
    "m2r",
]

DIST_REQUIREMENTS = ["twine"]

setup(
    name="ownhero-cloudhelpers",
    version="0.1a1",
    description="cloudhelpers: a collection of helper classes for use in cloud services.",
    long_description_content_type="text/markdown",
    long_description=LONG_DESCRIPTION,
    url="https://gitlab.com/OwnHeroNet/cloudhelpers",
    author="Sascha Just",
    author_email="sascha.just@own-hero.net",
    license="MIT",
    packages=find_packages(),
    namespace_packages=["ownhero"],
    install_requires=RUNTIME_REQUIREMENTS,
    tests_require=TEST_REQUIREMENTS,
    extras_require={
        "develop": DEV_REQUIREMENTS,
        "docs": DOC_REQUIREMENTS,
        "test": TEST_REQUIREMENTS,
        "dist": DIST_REQUIREMENTS,
        "redis": REDIS_REQUIREMENTS,
        "postgres": POSTGRES_REQUIREMENTS,
        "gfs": GOOGLE_FILESTORAGE_REQUIREMENTS,
    },
    zip_safe=False,
    platforms="Posix; MacOS X; Windows",
    python_requires=">=3.6",
    include_package_data=True,
    test_suite="nose.collector",
    classifiers=[
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Topic :: Internet",
        "Operating System :: OS Independent",
    ],
)

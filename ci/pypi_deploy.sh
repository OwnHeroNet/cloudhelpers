#!/bin/bash
rm -rf dist build .eggs *.egg-info \
&& pip3 install .[test,docs,dist] \
&& python3 setup.py sdist bdist_wheel \
&& nosetests --nocapture \
&& upload --repository-url https://test.pypi.org/legacy/ dist/* \
&& twine upload --repository-url https://test.pypi.org/legacy/ dist/* \
&& twine upload dist/*

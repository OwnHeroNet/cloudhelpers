"""
This module handles interaction with the SQL database.
"""

from contextlib import contextmanager

try:
    from psycopg2.pool import SimpleConnectionPool
except ImportError:
    pass

from ..config import Config
from .database import Database
from ..logger import LOGGER


class PostgresDatabase(Database):

    DEFAULT_HOST = "postgres"
    DEFAULT_PORT = 5432
    DEFAULT_USER = "postgres"
    DEFAULT_DATABASE = "postgres"

    def __init__(self, config: Config):
        super().__init__()

        LOGGER.info(
            (
                "Connecting to host=%s port=%s with user=%s "
                "on database=%s with ssl_mode=%s and setting "
                "search_path to schema %s."
            ),
            config.get(
                "database", "host", default=PostgresDatabase.DEFAULT_HOST
            ),
            config.get(
                "database", "port", default=PostgresDatabase.DEFAULT_PORT
            ),
            config.get(
                "database", "user", default=PostgresDatabase.DEFAULT_USER
            ),
            config.get(
                "database",
                "database",
                default=PostgresDatabase.DEFAULT_DATABASE,
            ),
            config.get("database", "connections", "ssl", default="disable"),
            config.get("database", "schema", default="public"),
        )
        self.pool = SimpleConnectionPool(
            minconn=0,
            maxconn=config.get("database", "connections", "max", default=10),
            dsn=f"""host={config.get("database","host", default=PostgresDatabase.DEFAULT_HOST)}
                   port={config.get("database","port", default=PostgresDatabase.DEFAULT_PORT)}
                   user={config.get("database","user", default=PostgresDatabase.DEFAULT_USER)}
                   dbname={config.get("database","database", default=PostgresDatabase.DEFAULT_DATABASE)}
                   password={config.get("database","password")}
                   sslmode={config.get("database","connections", "ssl", default="disable")}
                   """,
            options=f'-c search_path="{config.get("database","schema", default="public")}"',
        )

    @contextmanager
    def get_cursor(self):
        con = self.pool.getconn()
        try:
            yield con.cursor()
        finally:
            con.commit()
            self.pool.putconn(con)

    @contextmanager
    def get_connection(self):
        con = self.pool.getconn()
        try:
            yield con
        finally:
            self.pool.putconn(con)

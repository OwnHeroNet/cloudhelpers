"""
This module defines the :class:`Database` base class.
"""
from abc import ABC
from contextlib import contextmanager


class Database(ABC):
    """
    SQL database interface.
    """

    @contextmanager
    def get_cursor(self):
        """
        Contextmanager that provides a cursor for a transaction.

        Requests a connection from the pool, yields a cursor object and
        releases the connection back to the pool once the context is left.
        """

    @contextmanager
    def get_connection(self):
        """
        Contextmanager that provides a connection object.

        Requests a connection from the pool, yields it and releases the
        connection back to the pool once the context is left.
        """

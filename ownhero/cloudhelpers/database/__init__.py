from ..config import Config
from .postgres import PostgresDatabase


def Database(config: Config):
    if config.get("database", "type", default="postgres") == "postgres":
        return PostgresDatabase(config)
    else:
        raise RuntimeError("Invalid database config.")

import socket
import time

from .logger import LOGGER


def wait_for_dependency_service(service, host, port):
    LOGGER.info("Waiting for %s (%s:%s) to be ready.", service, host, port)
    while True:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect((host, port))
            break
        except ConnectionRefusedError:
            time.sleep(0.1)
            continue
        except RuntimeError as error:
            LOGGER.error(error)
            return
        finally:
            sock.close()
    LOGGER.info("%s (%s:%s) available.", service, host, port)

from json import dumps, loads
from multiprocessing import Process

from ..config import Config
from ..logger import LOGGER
from .pubsub import Message, PublisherClient

class NoOptPublisherClient(PublisherClient):
    """
    Specialization of :class:`PublisherClient`.

    Implements an interface to the Redis pubsub publisher methods.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        


    def publish(self, payload, **kwargs):
        pass
        
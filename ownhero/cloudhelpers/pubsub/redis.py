"""
This module implements the :class:`ownhero.cloudhelpers.pubsub.RedisPubSub`
class which provides an implementation of the ABC
:class:`ownhero.cloudhelpers.pubsub.PubSub` and provides functionality to
use a Redis cache as a pub/sub provider.

In contrast to e.g. Google's PubSub, Redis is non-persistent, i.e. messages
are fire and forget.
"""
from json import dumps, loads
from multiprocessing import Process

from ..config import Config
from ..logger import LOGGER
from .pubsub import Message, PublisherClient, SubscriberClient

try:
    import redis
except ImportError:
    pass


class TopicMixin:
    """
    Mixin to compute a topic path.
    """

    def __topic_path__(self, project, topic):
        return f"{project}/{topic}"


class RedisPublisherClient(PublisherClient, TopicMixin):
    """
    Specialization of :class:`PublisherClient`.

    Implements an interface to the Redis pubsub publisher methods.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        self.redis = redis.Redis(
            host=config.get("redis", "host", default="localhost"),
            port=config.get("redis", "port", default=6379),
        )
        self.topic_path = self.__topic_path__(
            config.get("telemetry", "project_id"),
            config.get("telemetry", "topic_name"),
        )

    def publish(self, payload, **kwargs):
        message = Message(data=payload, **kwargs)
        self.redis.publish(self.topic_path, dumps(message))


class RedisSubscriberClient(SubscriberClient, TopicMixin):
    """
    Specialization of :class:`SubscriberClient`.

    Implements an interface to the Redis pubsub subscriber methods.

    Subscribing to Redis pubsub is blocking. Hence, every subscription will
    spawn a new process which are collected and terminated once the instance
    of this class is freed by the GC.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        self.subscriber_processes = []
        self.redis = redis.Redis(
            host=config.get(["redis", "host"]),
            port=config.get(["redis", "port"]),
        )

    def __del__(self):
        for proc in self.subscriber_processes:
            proc.kill()

    @staticmethod
    def __listen__(redis_server, path, callback):
        pubsub = redis_server.pubsub()
        pubsub.subscribe([path])
        LOGGER.info("New subscription to %s", path)
        for item in pubsub.listen():
            if item["type"] == "message":
                payload = loads(item["data"])
                callback(
                    Message(data=payload["data"], **payload["attributes"])
                )

    def subscribe(self, project, topic, callback):
        path = self.__topic_path__(project, topic)
        process = Process(
            target=RedisSubscriberClient.__listen__,
            args=(self.redis, path, callback),
        )
        self.subscriber_processes.append(process)
        process.start()

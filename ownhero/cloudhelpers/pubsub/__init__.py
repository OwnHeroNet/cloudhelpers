from ..config import Config
from .google import GoogleSubscriberClient, GooglePublisherClient
from .redis import RedisPublisherClient, RedisSubscriberClient
from .noopt import NoOptPublisherClient
from ..exceptions import CloudHelpersError


def PublisherClient(config: Config):
    provider = config.get("telemetry", "provider", default="noopt")

    if provider == "noopt":
        return NoOptPublisherClient(config)
    elif provider == "redis":
        return RedisPublisherClient(config)
    elif provider == "gcloud":
        return GooglePublisherClient(config)
    else:
        raise CloudHelpersError(
            f"No pubsub provider configured or provider unsupported: {provider}"
        )


def SubscriberClient(config: Config):
    provider = config.get("telemetry", "provider", default="redis")

    if provider == "redis":
        return RedisSubscriberClient(config)
    elif provider == "gcloud":
        return GoogleSubscriberClient(config)
    else:
        raise CloudHelpersError(
            f"No pubsub provider configured or provider unsupported: {provider}"
        )

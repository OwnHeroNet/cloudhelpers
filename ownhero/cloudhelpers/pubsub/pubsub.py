"""
The pubsub module emits telemetry to a pubsub system.
"""
from abc import ABC, abstractmethod
from datetime import datetime
from json import dumps
from types import FunctionType

from ..config import Config


class Message(dict):
    """
    A message object that is provided when handling new data
    in a callback for subscriptions.
    """

    def __init__(self, data, **kwargs):
        super().__init__({"data": data, "attributes": kwargs})

    def __str__(self):
        return dumps(self)

    def __repr__(self):
        return (
            f"(data={self.get('data')}, "
            f"attributes={[(k, v) for k,v in self.get('attributes').items()]})"
        )

    def ack(self):
        pass


class SubscriberClient(ABC):
    """
    The subscriber base class serves as an interface for all
    implemented Pub/Sub providers.
    """

    def __init__(self, _config: Config):
        pass

    @abstractmethod
    def subscribe(self, project, topic, callback):
        """
        Subscribes to the given stream. The ``callback`` is called
        for every new arriving payload.

        :param func(Message) callback: callback function
        """


class PublisherClient(ABC):
    """
    The publisher base class serves as an interface for all
    implemented Pub/Sub providers.
    """

    def __init__(self, config: Config):
        self.protocol_version = config.get("telemetry", "proto")

        self.sent_success = 0
        self.sent_fatal = 0
        self.callbacks = dict()

    def register(self, event, callback):
        """
        Registers a callback for a given event, such as `started`, `success`,
        `failed`, `fatal`, `retrying`. If any of those is triggered, the callback
        will be invoked using `data` and `attributes` as parameters.
        """
        if isinstance(callback, FunctionType) and event in [
            "started",
            "success",
            "failed",
            "fatal",
            "retrying",
        ]:
            if not event in self.callbacks:
                self.callbacks[event] = []
            self.callbacks[event].append(callback)

    def send(self, data, mtype="event", **kwargs):
        """
        Sends a message to the message queue system.

        :param data: contains a JSON serialized object (payload).

        :param mtype: defines the type of the message (default: `'event'`).

        :param kwargs: keyword list of additional attributes to be attached
            to the message.
        """
        if isinstance(data, str):
            payload = dumps({"payload": data}).encode("utf-8")
        elif isinstance(data, dict):
            payload = dumps(data).encode("utf-8")
        else:
            payload = data

        self.publish(
            payload,
            type=mtype,
            timestamp=str(datetime.utcnow().timestamp()),
            proto=self.protocol_version,
            **kwargs,
        )

    @abstractmethod
    def publish(self, payload, **kwargs):
        """
        Publishes a new payload to the configured stream.

        :param payload: JSON serializable data
        :kwargs: attributes to the :class:`Message` object.
        """

    def started(self, data, **kwargs):
        """
        Notify pub/sub about a started task.

        :param data: the JSON data object of the task this event refers to.
            This can be `None`.

        :param kwargs: keyword list of additional attributes that are attached
            to the message, e.g. `job_guid='abc'`.
        """
        self.send(data, event="started", state="pending", **kwargs)
        if "started" in self.callbacks:
            for callback in self.callbacks["started"]:
                callback(data, **kwargs)

    def success(self, data, **kwargs):
        """
        Notify pub/sub about are succesfully completed task.

        :param data: the JSON data object of the task this event refers to.
            This can be `None`.

        :param kwargs: keyword list of additional attributes that are attached
            to the message, e.g. `job_guid='abc'`.
        """
        self.sent_success += 1
        self.send(data, event="completed", state="success", **kwargs)
        if "success" in self.callbacks:
            for callback in self.callbacks["success"]:
                callback(data, **kwargs)

    def failed(self, data, **kwargs):
        """
        Notify pub/sub about a failed task.

        :param data: the JSON data object of the task this event refers to.
            This can be `None`.

        :param kwargs: keyword list of additional attributes that are attached
            to the message, e.g. `job_guid='abc'`.
        """
        self.send(data, event="failed", state="pending", **kwargs)
        if "failed" in self.callbacks:
            for callback in self.callbacks["failed"]:
                callback(data, **kwargs)

    def fatal(self, data, **kwargs):
        """
        Notify pub/sub about a fatally failed task.

        :param data: the JSON data object of the task this event refers to.
            This can be `None`.

        :param kwargs: keyword list of additional attributes that are attached
            to the message, e.g. `job_guid='abc'`.
        """
        self.sent_fatal += 1
        self.send(data, event="disbanded", state="failed", **kwargs)
        if "fatal" in self.callbacks:
            for callback in self.callbacks["fatal"]:
                callback(data, **kwargs)

    def retrying(self, data, **kwargs):
        """
        Notify pub/sub about an initiated retry of a task.

        :param data: the JSON data object of the task this event refers to.
            This can be `None`.

        :param kwargs: keyword list of additional attributes that are attached
            to the message, e.g. `job_guid='abc'`.
        """
        self.send(data, type="event", event="retry", state="pending", **kwargs)
        if "retrying" in self.callbacks:
            for callback in self.callbacks["retrying"]:
                callback(data, **kwargs)

    def trigger(self, data, trigger, **kwargs):
        """
        Sends a trigger event with trigger type `trigger`.
        """
        self.send(data, mtype="trigger", trigger=trigger, **kwargs)

"""
This module implements the :class:`ownhero.cloudhelpers.pubsub.GooglePubSub`
class which provides an implementation of the ABC
:class:`ownhero.cloudhelpers.pubsub.PubSub` and provides functionality to
use Google as a pub/sub provider.

In contrast to e.g. Redis' PubSub, Google provides persistent streams on the
backend.
"""
from functools import partial

from ..config import Config
from ..logger import LOGGER
from .pubsub import Message, PublisherClient, SubscriberClient

try:
    from google.cloud import pubsub_v1 as gps
except ImportError:
    pass


class GoogleMessage(Message):
    def __init__(self, data, google_message, **kwargs):
        super().__init__(data, **kwargs)
        self.google_message = google_message

    def ack(self):
        self.google_message.ack()


class TopicMixin:
    """
    Mixin to compute a topic path.
    """

    def __topic_path__(self, project, topic):
        if hasattr(self, "subscriber"):
            return self.subscriber.subscription_path(project, topic)

        if hasattr(self, "publisher"):
            return self.publisher.topic_path(project, topic)

        return f"{project}/{topic}"


class GooglePublisherClient(PublisherClient, TopicMixin):
    """
    Specialization of :class:`PublisherClient`.

    Implements an interface to Google's Pub/Sub.
    This still requires the ``GOOGLE_APPLICATION_CREDENTIALS`` environment
    variable to be set.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        self.publisher = gps.PublisherClient()

        self.topic_path = self.__topic_path__(
            config.get("telemetry", "project_id"),
            config.get("telemetry", "topic_name"),
        )
        self.topic_name = config.get("telemetry", "topic_name")

    def publish(self, payload, **kwargs):
        future = self.publisher.publish(
            self.topic_path, data=payload, **kwargs
        )

        future.add_done_callback(self.telemetry_callback)

    def telemetry_callback(self, message_future):
        """
        Handles the message callback and potential errors.
        By default, this logs an error if there happens to be a 30s timeout.

        :param message_future: the future message object
        """
        # When timeout is unspecified, the exception method waits indefinitely.
        if message_future.exception(timeout=30):
            LOGGER.error(
                "Publishing message on %s threw an exception %s.",
                self.topic_name,
                message_future.exception(),
            )


class GoogleSubscriberClient(SubscriberClient, TopicMixin):
    """
    Specialization of :class:`SubscriberClient`.

    Implements an interface to Google's Pub/Sub.
    This still requires the ``GOOGLE_APPLICATION_CREDENTIALS`` environment
    variable to be set.
    """

    def __init__(self, config: Config):
        super().__init__(config)
        self.subscriber = gps.SubscriberClient()

    @staticmethod
    def __callback__(message, callback):
        message__ = GoogleMessage(
            data=message.data, google_message=message, **message.attributes
        )
        callback(message__)

    def subscribe(self, project, topic, callback):
        callback__ = partial(
            GoogleSubscriberClient.__callback__, callback=callback
        )
        self.subscriber.subscribe(
            self.__topic_path__(project, topic), callback=callback__
        )

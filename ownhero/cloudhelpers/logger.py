"""
This module provides a logging object `LOGGER` that is eagerly initialized.
If no log configuration is present, one will be generated on the fly using
the sample configuration in `assets/log.conf.example`.
"""

import logging
import logging.config
import os
from pathlib import Path

import pkg_resources


class MozLogger:
    """
    Wraps an instance of the root logger, featuring lazy loading of the log configuration.
    """

    def __init__(self, logger: logging.Logger = None, *_args, **_kwargs):
        self.logger = logger

    def __getattribute__(self, name):
        if object.__getattribute__(self, "logger") is None:
            object.__getattribute__(self, "load")()
        return object.__getattribute__(
            object.__getattribute__(self, "logger"), name
        )

    def load(self, *_args, **kwargs):
        """
        Loads the log config. Provides a default config if the config cannot be found.
        """
        env_var = kwargs.get("env_var", "LOG_CONFIG")
        config_file = os.getenv(env_var)
        file_name = kwargs.get("config_file", "log.conf")

        # Environment variable not set.
        # Lookup usual file locations
        if config_file is None:
            locations = [Path("."), Path.home(), Path("/etc")]
            for location in locations:
                path = Path(location, file_name)
                if path.is_file():
                    config_file = str(path)
                    break

        # Could not find config.
        # Create default config from assets and raise error.
        # make sure there is a log configuration
        if config_file is None:
            config_file = str(Path(".", file_name))
            with open(config_file, "wb") as cfile:
                config_str = pkg_resources.resource_string(
                    "ownhero.cloudhelpers", "assets/log.conf.example"
                )

                cfile.write(config_str)

        logging.config.fileConfig(config_file)
        self.logger = logging.getLogger(__package__)


LOGGER = MozLogger()

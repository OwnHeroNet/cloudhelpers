import datetime
import uuid
from .config import Config

try:
    import redis
except ImportError:
    pass


class KWMixin:
    def set(self, key, value):
        full_key = self.get_key(key)
        # this should actually use duck typing
        if isinstance(value, (bytes, int, float, str)):
            self._r_.set(full_key, value)
        elif isinstance(value, set):
            with self._r_.pipeline(shard_hint=full_key + "_shard") as p:
                p.watch(full_key)
                p.multi()
                p.delete(full_key)
                p.sadd(full_key, *value)
                p.execute()
                p.unwatch()
        elif isinstance(value, (list, tuple)):
            with self._r_.pipeline(shard_hint=full_key + "_shard") as p:
                p.watch(full_key)
                p.multi()
                p.delete(full_key)
                p.rpush(full_key, *value)
                p.execute()
                p.unwatch()
        elif isinstance(value, dict):
            with self._r_.pipeline(shard_hint=full_key + "_shard") as p:
                p.watch(full_key)
                p.multi()
                p.delete(full_key)
                p.hmset(full_key, value)
                p.execute()
                p.unwatch()
        else:
            self._r_.set(full_key, str(value))

    def get(self, key, default=None):
        full_key = self.get_key(key)
        # this should actually use duck typing
        if not self._r_.exists(full_key):
            return default

        value_type = self.__redis__.type(full_key)
        if value_type == b"string":
            return self.__redis__.get(full_key).decode("utf-8")
        elif value_type == b"set":
            return {
                x.decode("utf-8") for x in self.__redis__.smembers(full_key)
            }
        elif value_type == b"list":
            return [
                x.decode("utf-8")
                for x in self.__redis__.lrange(full_key, 0, -1)
            ]
        elif value_type == b"hash":
            return dict(
                zip(
                    (
                        key.decode("utf-8")
                        for key in self.__redis__.hkeys(full_key)
                    ),
                    (
                        value.decode("utf-8")
                        for value in self.__redis__.hvals(full_key)
                    ),
                )
            )
        else:
            raise RuntimeError(f"Unknown redis data type {value_type}.")


class ServiceState(KWMixin):
    instance_key = "instances"

    def __init__(self, config: Config, **_kwargs):
        self.__config__ = config
        self.service_name = config.get("service", "name", default=__package__)
        self.namespace = config.get("service", "namespace", default="_default")

        self.__redis__: redis.Redis
        self.service_db: int

        self.__get_service_db__()

    @property
    def _r_(self):
        return self.__redis__

    @property
    def config(self):
        return dict(
            host=self.__config__.get("redis", "host", default="redis"),
            port=self.__config__.get("redis", "port", default=6379),
            db=self.service_db,
            password=self.__config__.get("redis", "password"),
        )

    def __get_service_db__(self):
        r = redis.Redis(
            host=self.__config__.get("redis", "host", default="redis"),
            port=self.__config__.get("redis", "port", default=6379),
            db=0,
            password=self.__config__.get("redis", "password"),
        )

        id_field = f"{self.service_name}_database"
        db_id = r.get(id_field)

        # perform double locking
        if db_id is None:
            with r.lock("db_id_lock", timeout=10_000):
                db_id = r.get(id_field)

                if db_id is None:
                    seq = r.incr("db_id")
                    db_id = seq
                    r.set(id_field, db_id)

        self.__redis__ = redis.Redis(
            host=self.__config__.get("redis", "host", default="redis"),
            port=self.__config__.get("redis", "port", default=6379),
            db=db_id,
            password=self.__config__.get("redis", "password"),
        )
        self.service_db = db_id

    def get_key(self, key):
        return self.namespace + ":" + key

    def register(self, **kwargs):
        instance_state = InstanceState(self, **kwargs)
        self.__redis__.sadd(
            self.get_key(ServiceState.instance_key), str(instance_state.guid)
        )
        return instance_state

    def unregister(self, guid):
        for key in self.__redis__.scan_iter(f"{self.namespace}.{guid}:*"):
            self.__redis__.delete(key)
        self.__redis__.srem(self.get_key(ServiceState.instance_key), guid)

    def prune(self, delta: datetime.timedelta):
        now = datetime.datetime.utcnow().timestamp()

        for key in self.__redis__.scan_iter(
            match=f"{self.namespace}.*:{InstanceState.last_access_key}"
        ):
            if (
                datetime.datetime.fromtimestamp(float(self.__redis__.get(key)))
                < now - delta
            ):
                guid_str = key[len(self.namespace) : len(self.namespace) + 36]
                self.unregister(guid_str)


class InstanceState(KWMixin):
    last_access_key = "last_access"
    start_time_key = "start_time"

    def __init__(self, service_state: ServiceState, **kwargs):
        self.__redis__ = service_state.__redis__
        self.__guid__ = uuid.uuid4()
        self.__service_state__ = service_state

        self.start_time = kwargs.get("start_time", datetime.datetime.utcnow())

    @property
    def start_time(self):
        return datetime.datetime.fromtimestamp(
            float(
                self.__redis__.get(self.get_key(InstanceState.start_time_key))
            )
        )

    @property
    def namespace(self):
        return self.__service_state__.namespace

    @start_time.setter
    def start_time(self, start_time: datetime.datetime):
        self._r_.set(
            self.get_key(InstanceState.start_time_key), start_time.timestamp()
        )

    def __del__(self):
        self.__service_state__.unregister(self.guid)

    @property
    def guid(self):
        return str(self.__guid__)

    def get_key(self, key):
        return f"{self.namespace}.{self.guid}:{key}"

    @property
    def _r_(self):
        self.__redis__.set(
            self.get_key(InstanceState.last_access_key),
            str(datetime.datetime.utcnow().timestamp()),
        )
        return self.__redis__

import json
import operator
import os
import warnings
from functools import reduce
from pathlib import Path

from yaml import load

from ..logger import LOGGER
from .node import ConfigNode, ConfigNodeEncoder

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class Config:
    def __init__(self, **kwargs):
        """
        Initializes the inner configuration object if one is given.
        Otherwise, the first subscription will trigger the lazy
        loading mechanism.
        """
        config = dict()
        name = kwargs.get("name", "cloud").upper()
        env_var = name if "_config" in name else f"{name}_CONFIG"
        config_file = os.getenv(env_var)
        file_name = (
            os.path.basename(config_file)
            if env_var in os.environ
            else kwargs.get("config_file", f"{name.lower()}_config.yml")
        )

        # Environment variable not set.
        # Lookup usual file locations
        if config_file is None:
            locations = [Path("."), Path.home(), Path("/etc")]
            LOGGER.warning(
                "%s not set. Looking for config %s in %s",
                env_var,
                file_name,
                [str(x) for x in locations],
            )

            for location in locations:
                path = Path(location, file_name)
                LOGGER.debug("Checking %s", path)
                if path.is_file():
                    config_file = str(path)
                    break

        # Could not find config.
        # Create default config from assets and raise error.
        if config_file is None:
            warnings.warn("Missing configuration file.")
        elif not Path(config_file).is_file():
            # Config was set but is not an (accessible) file.
            warnings.warn(
                f"Could not read config from {config_file}. No such file."
            )
        else:
            # Read the configuration.
            with open(config_file, "r") as cfile:
                LOGGER.info("Reading config from '%s'.", config_file)
                try:
                    config.update(load(cfile, Loader=Loader))
                except RuntimeError as err:
                    raise RuntimeError("Config is no valid JSON file.", err)

        # Check for ENV variables
        for env_key, env_value in os.environ.items():
            if env_key.startswith(f"{name}_"):
                config_key = env_key[len(name) + 1 :]
                if config_key.islower():
                    try:
                        config_keys = config_key.split("_")
                        func = lambda acc, key: [
                            operator.setitem(acc, key, acc.get(key, dict())),
                            acc.get(key),
                        ][-1]
                        reduce(func, config_keys[:-1], config)
                        node = reduce(
                            operator.getitem, config_keys[:-1], config
                        )
                        node[config_keys[-1]] = env_value
                        LOGGER.info(
                            "Overwriting from ENV variable %s.", env_key
                        )
                    except (KeyError, TypeError):
                        LOGGER.warn("Ignoring ENV variable %s.", env_key)

        # update with values supplied in the constructor
        self.__root__ = ConfigNode(name, config)

    def __getattribute__(self, name):
        return object.__getattribute__(
            object.__getattribute__(self, "__root__"), name
        )

    def __getitem__(self, key):
        return object.__getattribute__(self, "__root__").__getitem__(key)

    def __iter__(self):
        return object.__getattribute__(self, "__root__").__iter__()

    def __str__(self):
        return json.dumps(
            object.__getattribute__(self, "__root__"),
            cls=ConfigNodeEncoder,
            sort_keys=True,
            indent=True,
        )

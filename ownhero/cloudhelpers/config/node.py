import json
import operator
from functools import reduce

from yaml import dump

try:
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Dumper


class ConfigNode:
    def __init__(self, key, entries, parent=None):
        super().__init__()
        self.__key__ = key
        self.__parent__ = parent
        self.__entries__ = dict()

        for k, v in entries.items():
            if ConfigNode.__is_dict__(v):
                self.__entries__[k] = ConfigNode(k, v, parent=self)
            else:
                self.__entries__[k] = v

    @staticmethod
    def __is_sequence__(arg):
        return not hasattr(arg, "strip") and (
            hasattr(arg, "__getitem__") or hasattr(arg, "__iter__")
        )

    @staticmethod
    def __is_dict__(arg):
        return hasattr(arg, "__getitem__") and hasattr(arg, "keys")

    def get_anchor(self):
        node = self
        anchor = [self.__key__]
        while node.__parent__:
            anchor.append(node.__parent__.__key__)
            node = node.__parent__

        return anchor

    def get(self, *keys, **kwargs):
        ret = self.__get_by_keys__(keys)

        return ret if ret else kwargs.get("default", None)

    def __getitem__(self, key):
        if ConfigNode.__is_sequence__(key):
            return self.__get_by_keys__(key)
        return self.__entries__.__getitem__(key)

    def __get_by_keys__(self, keys):
        try:
            return reduce(operator.getitem, keys, self.__entries__)
        except (KeyError, TypeError):
            return None

    def keys(self):
        return self.__entries__.keys()

    def values(self):
        return self.__entries__.values()

    def items(self):
        return self.__entries__.items()

    def __iter__(self):
        return self.__entries__.__iter__()

    def __setitem__(self, key, value):
        # https://docs.python.org/3/library/exceptions.html#TypeError
        raise TypeError(
            "Config objects should not be modified after loading. Please change your config file."
        )

    def __delitem__(self, key):
        # https://docs.python.org/3/library/exceptions.html#TypeError
        raise TypeError(
            "Config objects should not be modified after loading. Please change your config file."
        )

    def __str__(self):
        return json.dumps(self.__entries__)

    @property
    def __json__(self):
        return json.dumps(self.__entries__, cls=ConfigNodeEncoder)

    @property
    def __yaml__(self):
        return dump(json.loads(self.__json__), Dumper=Dumper)


class ConfigNodeEncoder(json.JSONEncoder):
    def default(self, o):  # pylint: disable=E0202
        if isinstance(o, ConfigNode):
            return o.__entries__

        return json.JSONEncoder.default(self, o)

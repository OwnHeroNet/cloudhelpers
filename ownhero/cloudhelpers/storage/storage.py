import configparser
import json
import os
import pickle
from abc import ABC, abstractmethod
from pathlib import Path

from yaml import dump, load

from ..config import Config

try:
    from yaml import CLoader as Loader
    from yaml import CDumper as Dumper
except ImportError:
    from yaml import Loader
    from yaml import Dumper


class Storage(ABC):

    default_storage = "storage"

    def __init__(self, config: Config):
        self.cache_dir = config.get("cachedir", default="cache")

    @abstractmethod
    def download(self, bucket_name: str, blob_name: str, file_path=None):
        pass

    def media_type(self, blob_name, **kwargs):
        if "type" in kwargs:
            content_type = kwargs.get("type")
        else:
            suffix = Path(blob_name).suffix
            if suffix == ".pkl":
                content_type = "pickle"
            elif suffix == ".json":
                content_type = "json"
            elif suffix == ".yml":
                content_type = "yaml"
            elif suffix == ".txt":
                content_type = "plain"
            elif suffix == ".ini" or suffix == ".cfg":
                content_type = "config"
            else:
                content_type = "binary"

        return content_type

    def load(
        self,
        bucket_name: str,
        blob_name: str,
        keep_file: bool = True,
        **kwargs,
    ):
        """
        This function fetches a blob from GCS and loads it using :mod:`pickle`.

        :see: :func:`~cloudhelpers.storage.download`.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the actual blob.

        :param bool keep_file: whether or not the local intermediate should be
            kept or deleted after successful upload.

        :return: a tuple containing the unpickled object and the corresponding
            metadata from `blob.metadata`.

        :rtype: tuple(object, dict)
        """
        file_path, metadata = self.download(bucket_name, blob_name)

        content_type = self.media_type(blob_name, **kwargs)

        if content_type == "object" or content_type == "pickle":
            with open(file_path, "rb") as file_handle:
                obj = pickle.load(file_handle)
        elif content_type == "binary":
            with open(file_path, "rb") as file_handle:
                obj = file_handle.read()
        elif content_type == "json":
            with open(file_path, "r") as file_handle:
                obj = json.load(file_handle)
        elif content_type == "yaml":
            with open(file_path, "r") as file_handle:
                obj = load(file_handle, Loader=Loader)
        elif content_type == "plain":
            with open(file_path, "r") as file_handle:
                obj = file_handle.read()
        elif content_type == "config" or content_type == "ini":
            with open(file_path, "r") as file_handle:
                obj = configparser.ConfigParser()
                obj.read_file(file_handle)

        if not keep_file:
            os.remove(file_path)

        return obj, metadata

    @abstractmethod
    def upload(self, bucket_name: str, blob_name: str, file_path: str):
        pass

    def store(
        self,
        bucket_name: str,
        blob_name: str,
        obj: object,
        metadata: dict = None,
        keep_file: bool = False,
        **kwargs,
    ):
        """
        Pickles a given object and stores it to a blob in GCS. Also sets the
        metadata of the blob object if any is given.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the target blob.

        :param object obj: the object that should be stored.

        :param dict metadata: the metadata to be associated with the object.

        :param bool keep_file: whether or not to keep the local intermediate file.
        """

        dir_path = os.path.abspath(f"{self.cache_dir}/{bucket_name}")
        file_path = Path(f"{dir_path}/{bucket_name}/{blob_name}")
        os.makedirs(file_path.parent, exist_ok=True)

        content_type = self.media_type(blob_name, **kwargs)

        if content_type == "object" or content_type == "pickle":
            with open(file_path, "wb") as file_handle:
                pickle.dump(obj, file_handle)
        elif content_type == "binary":
            with open(file_path, "wb") as file_handle:
                file_handle.write(obj)
        elif content_type == "json":
            with open(file_path, "w") as file_handle:
                json.dump(obj, file_handle)
        elif content_type == "yaml":
            with open(file_path, "w") as file_handle:
                dump(obj, file_handle, Dumper=Dumper)
        elif content_type == "plain":
            with open(file_path, "w") as file_handle:
                file_handle.write(obj)
        elif content_type == "config" or content_type == "ini":
            with open(file_path, "w") as file_handle:
                if isinstance(obj, configparser.ConfigParser):
                    obj.write(file_path)
                else:
                    file_handle.write(obj)
        else:
            raise RuntimeError(f"Unknown media type {content_type}.")

        self.upload(bucket_name, blob_name, file_path)
        self.set_metadata(bucket_name, blob_name, metadata)

        if not keep_file:
            os.remove(file_path)

    @abstractmethod
    def rename(self, bucket_name: str, blob_name: str, new_name: str):
        pass

    @abstractmethod
    def set_metadata(self, bucket_name: str, blob_name: str, metadata: dict):
        pass

    @abstractmethod
    def get_metadata(self, bucket_name: str, blob_name: str):
        pass

    @abstractmethod
    def del_metadata(self, bucket_name: str, blob_name: str):
        pass

    @abstractmethod
    def copy(self, bucket_name, blob_name, new_bucket_name, new_blob_name):
        pass

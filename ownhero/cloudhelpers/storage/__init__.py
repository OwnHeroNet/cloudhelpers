from ..config import Config
from .google import GoogleCloudStorage
from .local import LocalStorage
from .storage import Storage as BaseStorage
from ..exceptions import CloudHelpersError


def Storage(config: Config):
    provider = config.get("storage", "provider", default="local")
    if provider == "local":
        return LocalStorage(config)
    elif provider == "gcloud":
        return GoogleCloudStorage(config)
    else:
        raise CloudHelpersError("Invalid storage config.")

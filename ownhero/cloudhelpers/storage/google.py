"""
This module contains functions to create, access,
modify, and delete blobs in GCS.
"""

import os
from contextlib import contextmanager

from ..config import Config
from .storage import Storage

try:
    from google.cloud import storage
except ImportError:
    pass


class GoogleCloudStorage(Storage):
    def __init__(self, config: Config):
        super(GoogleCloudStorage, self).__init__(config)
        self.storage_client = storage.Client()

    def download(self, bucket_name: str, blob_name: str, file_path=None):
        """
        This function fetches a blob from GCS and stores it to a local file.
        If no location is given, a file with the same name as the blob will be
        written to the cache directory specified in `CONFIG['cachedir']`.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the actual blob.

        :param str file_path: the location to write the file contents to (optional).

        :return: a tuple containing the path the file and the corresponding
            metadata from `blob.metadata`.

        :rtype: tuple(str, dict)
        """
        if file_path is None:
            file_path = os.path.abspath(f"{self.cache_dir}/{blob_name}")

        with self.get_handle(bucket_name, blob_name) as blob:
            blob.download_to_filename(file_path)
            return file_path, blob.metadata

    def upload(self, bucket_name: str, blob_name: str, file_path: str):
        """
        Uploads a given file to GCS.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the target blob.

        :param str file_path: the path to the local file.
        """
        with self.get_handle(bucket_name, blob_name) as blob:
            blob.upload_from_filename(file_path)

    def rename(self, bucket_name: str, blob_name: str, new_name: str):
        """
        Renames a given blob instance.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the actual blob.

        :param str new_name: the new filename of the blob.
        """
        with self.get_handle(bucket_name, blob_name) as blob:
            blob.bucket.rename_blob(blob, new_name)

    def set_metadata(self, bucket_name: str, blob_name: str, metadata: dict):
        """
        Updates the metadata of a specific blob in GCS.
        See the ``notebooks/blob_metadata_control.ipynb`` for metadata related examples.

        :param str bucket_name: the name of the bucket the blob is located in.
        :param str blob_name: the name of the actual blob.
        """
        with self.get_handle(bucket_name, blob_name) as blob:
            blob.metadata = metadata
            blob.update()

    def get_metadata(self, bucket_name: str, blob_name: str):
        """
        Gets the metadata of a specific blob in GCS.
        See the `notebooks/blob_metadata_control.ipynb` for metadata related examples.

        :param str bucket_name: the name of the bucket the blob is located in.
        :param str blob_name: the name of the actual blob.
        """
        with self.get_handle(bucket_name, blob_name) as blob:
            return blob.metadata

    def del_metadata(self, bucket_name: str, blob_name: str):
        """
        Updates the metadata of a specific blob in GCS.
        See the `notebooks/blob_metadata_control.ipynb` for metadata related examples.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the actual blob.
        """
        with self.get_handle(bucket_name, blob_name) as blob:
            blob.metadata = None
            blob.update()

    @contextmanager
    def get_handle(self, bucket_name: str, blob_name: str):
        """
        Creates a blob object handle that can be used to access the blob and its metadata.

        :param str bucket_name: the name of the bucket the blob is located in.

        :param str blob_name: the name of the actual blob.
        """
        storage_client = storage.Client()
        bucket = storage_client.get_bucket(bucket_name)
        blob = bucket.get_blob(blob_name)
        yield blob

    def copy(self, bucket_name, blob_name, new_bucket_name, new_blob_name):
        """Copies a blob from one bucket to another with a new name."""

        storage_client = storage.Client()
        source_bucket = storage_client.get_bucket(bucket_name)
        source_blob = source_bucket.blob(blob_name)
        destination_bucket = storage_client.get_bucket(new_bucket_name)

        new_blob = source_bucket.copy_blob(
            source_blob, destination_bucket, new_blob_name
        )

        return new_blob

import json
import os
import shutil
from pathlib import Path

from ..config import Config
from .storage import Storage


class LocalStorage(Storage):
    def __init__(self, config: Config, **kwargs):
        super(LocalStorage, self).__init__(config)

        if "basedir" in kwargs:
            self.base_dir = os.path.abspath(kwargs.get("basedir"))
        else:
            self.base_dir = os.path.abspath(
                config.get("storage", "basedir", default="storage")
            )

        self.metadata_dir = os.path.abspath(f"{self.base_dir}/.metadata")
        os.makedirs(self.base_dir, exist_ok=True)
        os.makedirs(self.metadata_dir, exist_ok=True)

    def download(self, bucket_name: str, blob_name: str, file_path=None):
        if file_path is None:
            file_path = os.path.abspath(
                f"{self.cache_dir}/{bucket_name}/{blob_name}"
            )

        path = Path(file_path)
        os.makedirs(path.parent, exist_ok=True)
        shutil.copy(f"{self.base_dir}/{bucket_name}/{blob_name}", path)

        metadata = None
        with open(
            os.path.abspath(f"{self.metadata_dir}/{bucket_name}/{blob_name}"),
            "r",
        ) as meta_stream:
            metadata = json.load(meta_stream)

        return file_path, metadata

    def upload(self, bucket_name: str, blob_name: str, file_path: str):
        target_path = Path(f"{self.base_dir}/{bucket_name}/{blob_name}")
        meta_path = Path(f"{self.metadata_dir}/{bucket_name}/{blob_name}")

        os.makedirs(target_path.parent, exist_ok=True)
        os.makedirs(meta_path.parent, exist_ok=True)

        shutil.copy(file_path, f"{self.base_dir}/{bucket_name}/{blob_name}")

        with open(
            f"{self.metadata_dir}/{bucket_name}/{blob_name}", "w"
        ) as f_stream:
            json.dump({}, f_stream)

    def rename(self, bucket_name: str, blob_name: str, new_name: str):
        os.rename(
            os.path.abspath(f"{self.base_dir}/{bucket_name}/{blob_name}"),
            os.path.abspath(f"{self.base_dir}/{bucket_name}/{new_name}"),
        )
        os.rename(
            os.path.abspath(f"{self.metadata_dir}/{bucket_name}/{blob_name}"),
            os.path.abspath(f"{self.metadata_dir}/{bucket_name}/{new_name}"),
        )

    def set_metadata(self, bucket_name: str, blob_name: str, metadata: dict):
        with open(
            f"{self.metadata_dir}/{bucket_name}/{blob_name}", "w"
        ) as f_stream:
            json.dump(metadata, f_stream)

    def get_metadata(self, bucket_name: str, blob_name: str):
        with open(
            os.path.abspath(f"{self.metadata_dir}/{bucket_name}/{blob_name}"),
            "r",
        ) as meta_stream:
            metadata = json.load(meta_stream)
            return metadata

    def del_metadata(self, bucket_name: str, blob_name: str):
        with open(
            f"{self.metadata_dir}/{bucket_name}/{blob_name}", "w"
        ) as f_stream:
            json.dump({}, f_stream)

    def copy(self, bucket_name, blob_name, new_bucket_name, new_blob_name):
        shutil.copy(
            f"{self.base_dir}/{bucket_name}/{blob_name}",
            f"{self.base_dir}/{new_bucket_name}/{new_blob_name}",
        )

from unittest import TestCase
from ownhero.cloudhelpers.config import Config


class TestConfig(TestCase):
    """
    Tests the cloudhelpers config class.
    """

    def test_validation(self):
        try:
            config = Config()
        except ConfigError:
            pass

        self.assertEqual("Sascha Just", config.get("contact", "name"))

        with self.assertRaises(TypeError) as _context:
            config["contact"] = "whoops"

        with self.assertRaises(TypeError) as _context:
            del config["contact"]

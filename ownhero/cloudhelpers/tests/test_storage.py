import shutil
import tempfile
from unittest import TestCase

from ownhero.cloudhelpers.storage import Storage, LocalStorage
from ownhero.cloudhelpers.config import Config


class TestStorage(TestCase):
    """
    Tests the cloudhelpers storage class.
    """

    test_storage = "test_storage"

    def setUp(self):
        self.config = Config(
            config=dict(
                storage=dict(
                    provider="local", basedir=TestStorage.test_storage
                )
            )
        )
        self.storage = Storage(self.config)
        self.bucket_name = "test_bucket"

    def tearDown(self):
        shutil.rmtree(
            f"{TestStorage.test_storage}/{self.bucket_name}",
            ignore_errors=True,
        )
        shutil.rmtree(
            f"{TestStorage.test_storage}/.metadata/{self.bucket_name}",
            ignore_errors=True,
        )
        shutil.rmtree(f"{self.storage.cache_dir}", ignore_errors=True)

    def test_type(self):
        self.assertIsInstance(self.storage, LocalStorage)

    def test_upload(self):
        file_content = """
        This is a test.
        We store plain text.
        """
        blob_name = "test_blob.txt"

        with (
            tempfile.NamedTemporaryFile(
                mode="w", prefix="upload_test_", suffix=".txt"
            )
        ) as file:
            file.write(file_content)
            file.flush()
            self.storage.upload(self.bucket_name, blob_name, file.name)

    def test_store_load_plain(self):
        file_content = """
        This is a test.
        We store plain text.
        """
        blob_name = "test_blob.txt"

        self.storage.store(
            self.bucket_name,
            blob_name,
            file_content,
            metadata=dict(something="pretty"),
        )

        retrieved_content, metadata = self.storage.load(
            self.bucket_name, blob_name
        )
        self.assertMultiLineEqual(file_content, retrieved_content)
        self.assertDictContainsSubset(dict(something="pretty"), metadata)

    def test_store_load_json(self):
        file_content = dict(a=12, b=30, d="test")
        blob_name = "test_blob.json"

        self.storage.store(
            self.bucket_name,
            blob_name,
            file_content,
            metadata=dict(something="ugly"),
        )

        retrieved_content, metadata = self.storage.load(
            self.bucket_name, blob_name
        )
        self.assertDictEqual(file_content, retrieved_content)
        self.assertDictContainsSubset(dict(something="ugly"), metadata)

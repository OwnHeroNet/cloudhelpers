# Contributing to gcshelpers

This library does not require any external resources other than the python libraries it depends on. If you find any
lacking functionality or encounter tasks in the mozintermittent ecosystem that are implemented in multiple places, feel
free to move them to the `gcshelpers` library.

The `gcshelpers` library is a python3 project. For all documentation and examples, we assume you are running `python:3.6` with `pip`
and `virtualenv`/`virtualenvwrapper`. If you are using any other package management, such as `conda`, you are on your
own.

## Documentation of the google.cloud python libraries

This library wraps functionality from the Google Cloud API.
The API documentation of the google-cloud libraries can be found [here](https://googleapis.github.io/google-cloud-python/latest/index.html).
Examples can be found [here](https://cloud.google.com/storage/docs/reference/libraries).

## Getting the source code

You can clone the repository from the gitlab project using either `ssh` or `https`:

```sh
git clone git@gitlab.com:mozintermittent/libs/gcshelpers.git
```

or

```sh
git clone https://gitlab.com/mozintermittent/libs/gcshelpers.git
```

## The project structure

There are currently 3 folders in the repository.

* `docs/` contains the sphinx documentation for this library.
* `gcshelpers/` contains the source code of the python module.
* `resources/` contains resources, such as images, logos, references, etc.

## Before you begin

There are a few things to consider before you get started.

### Cloud credentials

You also need credentials for a service account. These are generated using the Google Cloud Console in the Service
Account management. We suggest you get a developer token, i.e. service account credentials that provide you with access
to everything within the project. This makes life a lot easier when developing. However, during testing you should use a
dedicated service account to verify that the permissions are setup correctly. The credential file has to conform to the
follwing pattern `mozintermittent-*.json`.

### Virtualenv setup

The master project mozintermittent/when-life-gives-you-oranges> contains hooks for the virtual environments. We suggest
you install those hooks first (using the provided `install.sh` script). These will guarantee a running Cloud SQL Proxy,
as well as correctly setup Google App Credentials which are requied to run this service. You can find them in the
`scripts/virtualenv` directory.

#### Steps to setup the development environment

Before you install the hooks, you have to create and activate the virtual environment first. Here is a typical example
of a fresh setup.

```sh
# Clone the repository to a local folder
git clone git@gitlab.com:mozintermittent/libs/gcshelpers ~/Development/gcshelpers
# switch to the repository's directory
cd ~/Development/gcshelpers
# create a new virtual environment
mkvirtualenv gcshelpers
# set the project folder for the environment
setvirtualenvproject
# Install the wrapper hooks.
# These guarantee that your GOOGLE_APPLICATION_CREDENTIALS are set and the
# Google SQL Proxy is running, s.t. the tools can access the Cloud SQL database.
#
# In case you cloned the master repository (when-life-gives-you-oranges) and initialized submodules
# $PATH_TO_MASTER would typically be '../..' as libs are kept in `libs/` and services in `tools/`.
sh $PATH_TO_MASTER/scripts/virtualenv/install.sh
# install basic packages for linting, debugging and refactorying
pip install pylint autopep8 rope ipython
# deactivate the environment
deactivate
# ATTENTION: at this point, you should get a Google App Credential file and place it in the repository's top level directory.
# activate the environment again to check if everything works
workon gcshelpers
# install gcshelpers
pip install -e .
# test gcshelpers
python setup.py test
# You are all set and good to go.
```

## Source code documentation

We don't require any sophisticated documentation guidelines. However, we strongly encourage descriptive module, class
and function docstrings. As we generate the source documentation using `sphinx` we urge you to follow the schema below
when writing docstrings.

### Python docstring examples

```python
'''
:param my_param: this describes a parameter of a function or method
:return: this describes the return value of a function or method

:mod:`a.b` this links to a python module `a.b`
:class:`x.Y` this links to a python class `Y` in module `x`
:func:`~x.Y.z` This links to a method `z` within the class `Y` of module `x`.
:func:`~x.y` This links to a function `y` of the module `x`.
'''
```

## Building the documentation

After setting up your development environment, you can also build the documentation yourself.

```sh
workon gcshelpers
cd docs
# this installs the necessary requirements for sphinx to build the documentation
pip install -r requirements.txt
# issue the build command
make html
# open the documentation
open build/html/index.html
```

## Wiping the environment

If, for some reason, you need a clean start and don't want to reinitialize the environment, you can just wipe it:

```sh
# activate the venv (should also switch to the correct PWD)
workon gcshelpers
# delete the editable installation
rm -rf $(find . -name '*.egg-info')
# wipe
wipeenv
# reinstall dependencies
pip install pylint autopep8 rope ipython
pip install -e .
```
